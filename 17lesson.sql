-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`catalog`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`catalog` (
  `catalog_id` INT NOT NULL AUTO_INCREMENT,
  `heading` VARCHAR(45) NULL,
  `catalog_catalog_id` INT NOT NULL,
  PRIMARY KEY (`catalog_id`, `catalog_catalog_id`),
  INDEX `fk_catalog_catalog1_idx` (`catalog_catalog_id` ASC),
  CONSTRAINT `fk_catalog_catalog1`
    FOREIGN KEY (`catalog_catalog_id`)
    REFERENCES `mydb`.`catalog` (`catalog_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`book` (
  `book_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(40) NOT NULL,
  `authors` VARCHAR(40) NOT NULL,
  `rating` INT NULL,
  `catalog_catalog_id` INT NOT NULL,
  PRIMARY KEY (`book_id`, `catalog_catalog_id`),
  INDEX `fk_book_catalog1_idx` (`catalog_catalog_id` ASC),
  CONSTRAINT `fk_book_catalog1`
    FOREIGN KEY (`catalog_catalog_id`)
    REFERENCES `mydb`.`catalog` (`catalog_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `date_of_birth` DATE NULL,
  `place_of_birth` VARCHAR(45) NULL,
  `rating` INT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`bookmark`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`bookmark` (
  `book_book_id` INT NOT NULL,
  `user_user_id` INT NOT NULL,
  PRIMARY KEY (`book_book_id`, `user_user_id`),
  INDEX `fk_bookmark_user1_idx` (`user_user_id` ASC),
  CONSTRAINT `fk_bookmark_book1`
    FOREIGN KEY (`book_book_id`)
    REFERENCES `mydb`.`book` (`book_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_bookmark_user1`
    FOREIGN KEY (`user_user_id`)
    REFERENCES `mydb`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`password`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`password` (
  `user_user_id` INT NOT NULL,
  `password` INT NOT NULL,
  PRIMARY KEY (`user_user_id`),
  CONSTRAINT `fk_password_user`
    FOREIGN KEY (`user_user_id`)
    REFERENCES `mydb`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`reference`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`reference` (
  `book_book_id` INT NOT NULL,
  `reference_adress` VARCHAR(45) NULL,
  PRIMARY KEY (`book_book_id`),
  CONSTRAINT `fk_reference_book1`
    FOREIGN KEY (`book_book_id`)
    REFERENCES `mydb`.`book` (`book_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




-- SELECTS 


select model, speed, hd, price FROM pc
where price < 500
order by price;

 SELECT maker, type FROM product
 where type = 'printer'
 ORDER BY maker;
 
SELECT maker, type FROM product
WHERE type = 'PC'
ORDER BY maker DESC;

SELECT name FROM ships
WHERE class RLIKE '[o]$'
AND NOT  '[g].$';

SELECT maker, type, speed, hd FROM product
JOIN laptop ON product.model = laptop.model
WHERE hd > 9;

SELECT PC1.model AS model1, PC2.model AS model2, PC1.speed, PC1.ram FROM PC AS PC2
JOIN PC AS PC1
ON (PC1.speed = PC2.speed) AND (PC1.ram = PC2.ram)
WHERE PC1.code <> PC2.code;

SELECT * FROM PC
LEFT JOIN laptop
ON PC.speed = laptop.speed;

SELECT * FROM PC
RIGHT JOIN laptop
ON PC.speed = laptop.speed;

SELECT * FROM PC
FULL JOIN laptop
ON PC.speed = laptop.speed;

 SELECT * FROM PC
LEFT JOIN laptop
ON PC.speed = laptop.speed
WHERE PC.hd > 9;

SELECT * FROM laptop
LEFT JOIN (SELECT * FROM PC WHERE hd > 9) AS PC
ON PC.speed = laptop.speed;


