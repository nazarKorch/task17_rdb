DELIMITER $$
USE `17db`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `17db`.`student_BEFORE_INSERT` BEFORE INSERT ON `student` FOR EACH ROW
BEGIN
SET new.student_years_old = (new.student_admission - new.date_of_birth);
END$$
DELIMITER ;


SELECT class, displacement FROM classes
WHERE displacement > 39000
ORDER BY type;

SELECT model FROM pc
WHERE model RLIKE '[1]{2}|[1]%[1]|.[1].[1]|[1].[1].';

SELECT * FROM outcome
WHERE date BETWEEN '2001-03-00' AND '2001-04-00';

SELECT * FROM outcome_o
WHERE date LIKE '%-%-14%';

SELECT name FROM ships
WHERE name LIKE 'W%n' ;

SELECT name,launched FROM ships
WHERE name RLIKE '^[a-z]*[e][a-z]*[e][a-z]*$';

SELECT name,launched FROM ships
WHERE name RLIKE '^[a-z]*[^a]$';

 SELECT name FROM battles
WHERE name RLIKE '^[a-z]*[[:space:]][a-z]*[^c]$';

SELECT * FROM trip
WHERE time_out BETWEEN '1900-01-01 12:00:00' AND '1900-01-01 17:00:00';

select * from pass_in_trip
where place LIKE '%c';

select distinct l1.model model1, l2.model model2, l1.speed, l1.ram
FROM laptop l1, laptop l2
WHERE l1.speed = l2.speed AND l1.ram = l2.ram AND l1.model > l2.model;

SELECT  c1.country, c1.type, c2.type
FROM classes c1, classes c2
WHERE c1.country = c2.country AND c1.type <> c2.type AND c1.type > c2.type;

SELECT p.maker, p.model, pc.price FROM product p
JOIN pc
ON pc.model = p.model
WHERE p.type = 'pc';

SELECT s.name, c.displacement
FROM ships s, classes c
WHERE c.class = s.class;

SELECT b.name, b.date
FROM battles b JOIN outcomes o
ON b.name = o.battle
WHERE o.result = 'ok';

-- select name of passenger and name of used company

SELECT p.name, c.name company FROM
passenger p JOIN
(SELECT pas.id_psg, co.name FROM
pass_in_trip pas JOIN
(SELECT t.trip_no, com.name FROM trip t
JOIN company com
ON t.id_comp = com.id_comp) co
ON pas.trip_no = co.trip_no) c
ON p.id_psg = c.id_psg;

